import requests
from bs4 import BeautifulSoup
import string
import nltk
nltk.download('punkt')
nltk.download('cmudict')

# URL of the web page you want to scrape
url = "https://insights.blackcoffer.com/ai-in-healthcare-to-improve-patient-outcomes/"

# Send a GET request to the URL and get the response
response = requests.get(url)

# Check if the request was successful
if response.status_code == 200:
    # Parse the HTML content using BeautifulSoup
    soup = BeautifulSoup(response.content, "html.parser")

    # Find the specific div class
    div1 = soup.find("div", class_="td_block_wrap tdb_single_content tdi_126 td-pb-border-top td_block_template_1 td-post-content tagdiv-type")  # Replace "your-div-class" with the actual class name
    div = div1.find("div", class_="tdb-block-inner td-fix-index")  # Replace "your-div-class" with the actual class name

    if div:
        # Extract the paragraph text from the div
        paragraphs = div.find_all("p")
        paragraph_data = ""
        for p in paragraphs:
            paragraph_data += p.get_text()

        # Load the stop words from external text files
        with open("StopWords_Auditor.txt", "r",encoding="utf-8") as file1, open("StopWords_Currencies.txt", "r",encoding='iso-8859-1') as file2, open("StopWords_DatesandNumbers.txt", "r",encoding="utf-8") as file3, open("StopWords_Generic.txt", "r",encoding="utf-8") as file4 ,open("StopWords_GenericLong.txt", "r",encoding="utf-8") as file5 ,open("StopWords_Geographic.txt", "r",encoding="utf-8") as file6 ,open("StopWords_Names.txt", "r",encoding="utf-8") as file7:

            stop_words1 = file1.read().splitlines()
            stop_words2 = file2.read().splitlines()
            stop_words3 = file3.read().splitlines()
            stop_words4 = file4.read().splitlines()
            stop_words5 = file5.read().splitlines()
            stop_words6 = file6.read().splitlines()
            stop_words7 = file7.read().splitlines()
          
            stop_words = stop_words1 + stop_words2  + stop_words3 + stop_words4 + stop_words5 + stop_words6 + stop_words7
            
words = nltk.word_tokenize(paragraph_data)
filtered_words = [word for word in words if word.lower() not in stop_words]
filtered_paragraph_data = " ".join(filtered_words)
#print("\nParagraph data after removing stop words:")
#print(filtered_paragraph_data)
#print(stop_words)


# Read the positive words from a text file
with open("positive-words.txt", "r") as positive_file:
    positive_words = positive_file.read().splitlines()

# Read the negative words from a text file
with open("negative-words.txt", "r",encoding='iso-8859-1') as negative_file:
    negative_words = negative_file.read().splitlines()
# Filtered paragraph data
filtered_paragraph_data = filtered_words

# Lists to store positive and negative words matching with filtered paragraph data
positive_words_matching = []
negative_words_matching = []

# Loop through positive words dictionary
for word in positive_words:
    # Check if the word matches with any word in filtered paragraph data
    if word.lower() in filtered_paragraph_data:
        # Add the word to the list of words matching
        positive_words_matching.append(word)

# Loop through negative words dictionary
for word in negative_words:
    # Check if the word matches with any word in filtered paragraph data
    if word.lower() in filtered_paragraph_data:
        # Add the word to the list of words matching
        negative_words_matching.append(word)

# Print positive and negative words matching with filtered paragraph data
#print("Positive Words Matching:")
#print(positive_words_matching)
#print("Negative Words Matching:")
#print(negative_words_matching)

# List to store unmatched words from filtered paragraph data
unmatched_words = []

# Loop through filtered paragraph data
for word in filtered_paragraph_data:
    # Check if the word does not match with any word in positive or negative words dictionaries
    if word.lower() not in positive_words and word.lower() not in negative_words:
        # Add the word to the list of unmatched words
        unmatched_words.append(word)

# Print unmatched words from filtered paragraph data
#print("Unmatched Words from Filtered Paragraph Data:")
#print(unmatched_words)
# Positive Dictionary
positive_dict = positive_words

# Filtered Paragraph Data
filtered_paragraph_data = " ".join(filtered_paragraph_data)

# Find unmatched words in filtered paragraph data
unmatched_words = [word for word in filtered_paragraph_data if word not in positive_dict]

# Calculate Positive Score
positive_score = 0
for word in positive_dict:
    if word in filtered_paragraph_data:
        positive_score += 1



# Negative Dictionary
negative_dict = negative_words

# Filtered Paragraph Data
filtered_paragraph_data = " ".join(filtered_paragraph_data)

# Find unmatched words in filtered paragraph data
unmatched_words = [word for word in filtered_paragraph_data if word not in negative_dict]

# Calculate Negative Score
negative_score = 0
for word in negative_dict:
    if word in filtered_paragraph_data:
        negative_score += -1
# Calculate Polarity Score
polarity_score = (positive_score - negative_score) / ((positive_score + negative_score) + 0.000001)        
# Calculate Subjectivity Score
total_words = len(filtered_paragraph_data)
subjectivity_score = (positive_score + negative_score) / (total_words + 0.000001)

# Calculate Average Sentence Length
sentences = nltk.sent_tokenize(filtered_paragraph_data)
num_sentences = len(sentences)
words = nltk.word_tokenize(filtered_paragraph_data)
num_words = len(words)
average_sentence_length = num_words / num_sentences

# Calculate Percentage of Complex Words
complex_words = [word for word in words if word in unmatched_words]
num_complex_words = len(complex_words)
percentage_complex_words = (num_complex_words / num_words) * 100

# Calculate Fog Index
fog_index = 0.4 * (average_sentence_length + percentage_complex_words)
# Calculate Average Number of Words Per Sentence
average_num_words_per_sentence = num_words / num_sentences

        
print("Positive Score:", positive_score)
print("Negative Score:", negative_score)
print("Polarity Score:", polarity_score)
print("Subjectivity Score:", subjectivity_score)
print("Average Sentence Length:", average_sentence_length)
print("Percentage of Complex Words:", percentage_complex_words)
print("Fog Index:", fog_index)
print("Average Number of Words Per Sentence:", average_num_words_per_sentence)
